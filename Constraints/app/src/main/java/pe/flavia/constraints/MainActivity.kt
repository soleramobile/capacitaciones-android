package pe.flavia.constraints

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import pe.flavia.constraints.databinding.ActivityConstraintProgrammaticalBinding

class MainActivity : AppCompatActivity() {

    private lateinit var tv: AppCompatTextView
    private lateinit var binding: ActivityConstraintProgrammaticalBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
        //createTv()
        onClickEvents()
    }
    private fun bind(){
        this.binding = ActivityConstraintProgrammaticalBinding.inflate(layoutInflater)
        setContentView(this.binding.root)
    }

    private fun createTv(){
        this.tv = AppCompatTextView(this)
        tv.apply {
            this.id = View.generateViewId()
            this.layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT)
            this.text = "fsdgjsghskhdjksk"
        }
        this.binding.ctr.addView(this.tv)
        assignConstraints()
    }

    private fun assignConstraints(){
        val constraintSet = ConstraintSet()
        constraintSet.apply {
            this.clone(this@MainActivity.binding.ctr)
            this.connect(this@MainActivity.tv.id, ConstraintSet.TOP, this@MainActivity.binding.btn.id, ConstraintSet.BOTTOM)
            this.connect(this@MainActivity.tv.id, ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START)
            this.connect(this@MainActivity.tv.id, ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END)
            this.connect(this@MainActivity.tv.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 32)
            this.setVerticalBias(this@MainActivity.tv.id, 1f)
            this.applyTo(this@MainActivity.binding.ctr)
        }
    }

    private fun onClickEvents(){
        this.binding.btn.setOnClickListener {
            changeConstraints()
        }
    }

    private fun changeConstraints(){
        val constraintSet = ConstraintSet()
        constraintSet.apply{
            this.clone(this@MainActivity.binding.ctr)
            this.connect(this@MainActivity.binding.tvR.id, ConstraintSet.TOP, this@MainActivity.binding.btn2.id, ConstraintSet.BOTTOM)
            this.connect(this@MainActivity.binding.btn2.id, ConstraintSet.TOP, this@MainActivity.binding.btn.id, ConstraintSet.BOTTOM)
            this.applyTo(this@MainActivity.binding.ctr)
        }
    }
}