# CAPACITACIONES #

Capacitacion #1: Constraints


- Constraints Doc: https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintLayout

- Constraints Circular: https://www.journaldev.com/21366/android-constraint-layout-circular-positioning

- ConstraintSet Doc: https://developer.android.com/reference/androidx/constraintlayout/widget/ConstraintSet